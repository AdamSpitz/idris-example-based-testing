module ExampleBasedTesting.TestSuite

import ExampleBasedTesting.Test
import ExampleBasedTesting.TestStep


%default total
%access public export


-- I have a feeling that this is *already* a well-known thing whose name I should know.

data TestSuiteStructure a = Empty
                          | Singleton a
                          | Composite (TestSuiteStructure a) (TestSuiteStructure a)

implementation Functor TestSuiteStructure where
  map f s = case s of
    Empty => Empty
    Singleton a => Singleton (f a)
    Composite s1 s2 => Composite (map f s1) (map f s2)

implementation Foldable TestSuiteStructure where
  foldr func initialAcc s = case s of
    Empty => initialAcc
    Singleton a => func a initialAcc
    Composite s1 s2 => foldr func (foldr func initialAcc s2) s1

implementation Traversable TestSuiteStructure where
  traverse f s = case s of
    Empty => pure Empty
    Singleton a => map Singleton (f a)
    Composite s1 s2 => (pure Composite) <*> (traverse f s1) <*> (traverse f s2)

TestSuiteResult : Type
TestSuiteResult = TestSuiteStructure (String, SingleTestResult)




TestSuite : (m : Type -> Type) -> Type
TestSuite m = TestSuiteStructure (SingleTest m)

emptySuite : (Monad m) => TestSuite m
emptySuite = Empty

suiteFromSingleTest : (Monad m) => SingleTest m -> TestSuite m
suiteFromSingleTest = Singleton

implementation Monad m => Semigroup (TestSuite m) where
  (<+>) = Composite
    
implementation Monad m => Monoid (TestSuite m) where
  neutral = Empty

suiteFromSuites : (Foldable c, Monad m) => c (TestSuite m) -> TestSuite m
suiteFromSuites testSuites = concat testSuites

suiteFromTests : (Foldable c, Monad m) => c (SingleTest m) -> TestSuite m
suiteFromTests tests = concatMap suiteFromSingleTest tests

runTestSuite : (Monad m) => TestSuite m -> m TestSuiteResult
runTestSuite testSuite = traverse (\test => map (\r => (singleTestDescription test, r))
                                                (runSingleTest test))
                                  testSuite
