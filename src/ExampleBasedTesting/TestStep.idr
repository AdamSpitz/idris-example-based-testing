module ExampleBasedTesting.TestStep

import Prelude.Basics


%default total
%access public export


data SingleTestFailure = MkSingleTestFailure String

Show SingleTestFailure where
  show (MkSingleTestFailure s) = "MkSingleTestFailure " ++ show s

data SingleTestResult = SuccessfulSingleTestResult | FailedSingleTestResult SingleTestFailure

Show SingleTestResult where
  show SuccessfulSingleTestResult = "SuccessfulSingleTestResult"
  show (FailedSingleTestResult f) = "FailedSingleTestResult (" ++ (show f) ++ ")"



record TestStep (m : Type -> Type) (x : Type) where
  constructor MkTestStep
  produceFailureOrValue : (m (Either SingleTestFailure x))

pureTestStep : (Monad m) => x -> TestStep m x
pureTestStep x = MkTestStep (pure (Right x))

bindTestStep : (Monad m) => TestStep m x1 -> (x1 -> TestStep m x2) -> TestStep m x2
bindTestStep test f = MkTestStep $ do
  failureOrX <- produceFailureOrValue test
  case failureOrX of
    Left failure => pure (Left failure)
    Right x => produceFailureOrValue (f x)
  

implementation Monad m => Functor (TestStep m) where
  map f test = bindTestStep test (\x => pureTestStep (f x))

implementation Monad m => Applicative (TestStep m) where
  pure = pureTestStep
  test1 <*> test2 = bindTestStep test1 (\f =>
                      bindTestStep test2 (\arg =>
                        pure (f arg)))

implementation Monad m => Monad (TestStep m) where
  (>>=) = bindTestStep

ignoreOutputValue : (Monad m) => TestStep m x -> TestStep m ()
ignoreOutputValue testStep = do
  testStep
  pure ()



assertEqual : (Eq a, Show a, Monad m) => a -> a -> TestStep m ()
assertEqual a1 a2 =
  if a1 == a2 then
    MkTestStep (pure (Right ()))
  else
    MkTestStep (pure (Left (MkSingleTestFailure ((show a1) ++ " was not equal to " ++ (show a2)))))

assertNotEqual : (Eq a, Show a, Monad m) => a -> a -> TestStep m ()
assertNotEqual a1 a2 =
  if a1 == a2 then
    MkTestStep (pure (Left (MkSingleTestFailure ((show a1) ++ " was equal to " ++ (show a2)))))
  else
    MkTestStep (pure (Right ()))

assert : (Monad m) => {prop : Type} -> Dec prop -> TestStep m prop
assert dec = case dec of
  (Yes prf) => MkTestStep (pure (Right prf))
  (No contra) => MkTestStep (pure (Left (MkSingleTestFailure "assertion failure: tried to assert, but was proven false")))

deny : (Monad m) => {prop : Type} -> Dec prop -> TestStep m (prop -> Void)
deny dec = case dec of
  (Yes prf) => MkTestStep (pure (Left (MkSingleTestFailure "assertion failure: tried to deny, but was proven true")))
  (No contra) => MkTestStep (pure (Right contra))

assertSatisfies : (Monad m) => {predType : a -> Type} -> (pred : (v : a) -> Dec (predType v)) -> (v : a) -> TestStep m (predType v)
assertSatisfies pred v = case (pred v) of
  (Yes prf) => MkTestStep (pure (Right prf))
  (No contra) => MkTestStep (pure (Left (MkSingleTestFailure "assertion failure: tried to assert, but was proven false")))

denySatisfies : (Monad m) => {predType : a -> Type} -> (pred : (v : a) -> Dec (predType v)) -> (v : a) -> TestStep m ((predType v) -> Void)
denySatisfies pred v = case (pred v) of
  (Yes prf) => MkTestStep (pure (Left (MkSingleTestFailure "assertion failure: tried to deny, but was proven true")))
  (No contra) => MkTestStep (pure (Right contra))


