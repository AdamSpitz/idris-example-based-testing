module ExampleBasedTesting.Main

import Control.Monad.Identity
import ExampleBasedTesting.Test
import ExampleBasedTesting.TestRunner
import ExampleBasedTesting.TestStep
import ExampleBasedTesting.TestSuite


test1 : SingleTest Identity
test1 = testNamed "test1" $ do
  assertEqual 3 3
  pure ()

test2 : SingleTest Identity
test2 = testNamed "test2" $ do
  assertNotEqual 3 4
  pure ()

test3 : SingleTest Identity
test3 = testNamed "test3" $ do
  assert (nonEmpty (the (List Int) [1, 2, 3]))
  deny (nonEmpty (the (List Int) []))
  assertSatisfies nonEmpty (the (List Int) [1, 2, 3])
  denySatisfies nonEmpty (the (List Int) [])
  pure ()

export
runAllTests : IO ()
runAllTests = runTestSuiteAndPrintResults (suiteFromTests [test1, test2, test3]) (pure . runIdentity)
