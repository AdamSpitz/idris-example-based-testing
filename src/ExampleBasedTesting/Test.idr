module ExampleBasedTesting.Test

import ExampleBasedTesting.TestStep

%default total
%access public export



record SingleTest (m : Type -> Type) where
  constructor MkSingleTest
  desc : String
  singleTestStep : TestStep m ()

singleTestDescription : SingleTest m -> String
singleTestDescription = desc

testNamed : (Monad m) => String -> TestStep m x -> SingleTest m
testNamed desc step = MkSingleTest desc (ignoreOutputValue step)

runSingleTest : (Monad m) => SingleTest m -> m SingleTestResult
runSingleTest test = do
  failureOrX <- produceFailureOrValue (singleTestStep test)
  case failureOrX of
    Left failure => pure (FailedSingleTestResult failure)
    Right x => pure SuccessfulSingleTestResult
