module ExampleBasedTesting.TestRunner

import ExampleBasedTesting.TestStep
import ExampleBasedTesting.TestSuite

%access public export


printSingleTestResult : (String, SingleTestResult) -> IO ()
printSingleTestResult (desc, result) =
  putStrLn (desc ++ ": " ++ (case result of
                              SuccessfulSingleTestResult => "success"
                              FailedSingleTestResult (MkSingleTestFailure s) => "failure: " ++ s))

runTestSuiteAndPrintResults : (Monad m) => TestSuite m -> (m TestSuiteResult -> IO TestSuiteResult) -> IO ()
runTestSuiteAndPrintResults testSuite fnForIOTestSuiteResult = do
  putStrLn "About to run test suite."
  testSuiteResult <- fnForIOTestSuiteResult (runTestSuite testSuite)
  sequence (map printSingleTestResult (toList testSuiteResult))
  putStrLn "Test suite complete."
  pure ()
