# Idris example-based testing library




## Building and testing

nix-build
nix-shell --pure --command "idris --testpkg examplebasedtesting.ipkg"
nix-shell --pure --command "idris --clean examplebasedtesting.ipkg"


## To do

  - How do I put this online somewhere so that my other projects can depend on it?
    - Maybe I:
      - Put it on GitLab or GitHub.
      - Just update my local nixpkgs for now.
      - Eventually submit a pull request to nixpkgs, when I feel like the library is ready.
    - I think that should let me do dev work while still keeping things in separate projects and using normal dependencies between them.
  - Figure out the private/export/"public export" thing. I think I should probably be keeping things private by default. Maybe.
