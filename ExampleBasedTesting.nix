{ build-idris-package
, fetchFromGitHub
, lib
}:
build-idris-package  {
  name = "examplebasedtesting";
  version = "0.1.2";

  ipkgName = "examplebasedtesting";

  idrisDeps = [];

  src = ./.;

  meta = {
    description = "examplebasedtesting";
  };
}
